node default {
    include cron-puppet
    group { 'Love':
      ensure		=>'present',
	}
    user { 'chandani':
      ensure           => 'present',
      home             => '/home/chandani',
      comment           => 'Chandani Ananthula',
      groups            => 'Love',
      password         => 'abc',
      password_max_age => '99999',
      password_min_age => '0',
      shell            => '/bin/bash',
      uid              => '143',
    }
}
